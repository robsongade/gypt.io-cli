const fs = require("fs")
const path = require("path")

/**
 * Look ma, it's cp -R.
 * @param {string} src The path to the thing to copy.
 * @param {string} dest The path to the new copy.
 */
var copyRecursiveSync = function(src, dest, callback) {
    var exists = fs.existsSync(src);

    console.log('src', src, 'dest', dest)
    var stats = exists && fs.statSync(src);
    var isDirectory = exists && stats.isDirectory();
    const exist = fs.existsSync(dest + '/package.json');
    if (exist) {
        callback()

        return;
    }

    if (isDirectory) {
        fs.mkdirSync(dest);
        fs.readdirSync(src).forEach(function(childItemName) {
            copyRecursiveSync(path.join(src, childItemName),
                path.join(dest, childItemName));
        });
    } else {

        fs.copyFileSync(src, dest);
    }
    callback()
};


module.exports = {
    copy: copyRecursiveSync
}