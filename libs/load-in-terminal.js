// INCLUDES
const _childProcess = require('child_process'); // ES6 Syntax
function LoadInTerminal(command, callback, params) {

    // GLOBALS
    let exec = _childProcess.exec; // Or use 'var' for more proper 
    // semantics, though 'let' is 
    // true-to-scope
    let childProcess;
    if (params && callback) {

        childProcess = exec(
            command, { cwd: params.cwd },
            (error, stdout, stderr) => {
                if (error) {
                    // This won't show up until the process completes:
                    console.log('[ERROR]: "' + error.name + '" - ' + error.message);
                    console.log('[STACK]: ' + error.stack);

                    console.log(stdout);
                    console.log(stderr);
                    callback(); // Gulp stuff
                    return;
                }

                // Neither will this:
                console.log(stdout);
                console.log(stderr);
                callback(); // Gulp stuff
            }
        );


    } else {

        childProcess = exec(
            command,
            (error, stdout, stderr) => {
                if (error) {
                    // This won't show up until the process completes:
                    console.log('[ERROR]: "' + error.name + '" - ' + error.message);
                    console.log('[STACK]: ' + error.stack);

                    console.log(stdout);
                    console.log(stderr);
                    callback(); // Gulp stuff
                    return;
                }

                // Neither will this:
                console.log(stdout);
                console.log(stderr);
                callback(); // Gulp stuff
            }
        );
    }



    childProcess.stdout.on(
        'data',
        (data) => {
            // This will render 'live':
            console.log('[STDOUT]: ' + data);
        }
    );


    childProcess.stderr.on(
        'data',
        (data) => {
            // This will render 'live' too:
            console.log(data);
        }
    );

}


module.exports = LoadInTerminal