class Commands {
    list = ['new', "dev", 'serve'];
    commands = {}

    constructor() {

        var commands = process.argv
        var x = 0;
        commands.forEach(element => {
            if (this.commands[element]) {
                return
            }
            this.list.filter((c) => {
                if (c == element) {
                    var args = []
                    for (var y = x + 1; y <= commands.length; y++) {
                        if (commands[y])
                            args.push(commands[y])
                    }
                    this.commands[element] = args
                }

            });

            x++;
        });

    }

    get(command) {
        var get = this.commands[command]
        return get ? get : false
    }
}
module.exports = new Commands()