#!/usr/bin/env node

const { exec } = require("child_process");
const commands = require('./libs/commands');
const LoadInTerminal = require('./libs/load-in-terminal')


const events = require('events');
const listEvents = ["defaultsFiles"];

class GioEvent extends events {
    runs = []
    check(target) {
        var filter = listEvents.filter(v => v == target)

        if (!filter.length) {

            return;
        }
        if (!this.runs[target]) {
            this.runs.push(target)
        }
        var runs = this.runs


    }
}

const event = new GioEvent();

event.on('log', (data) => {
    console.log(data);
});
event.on('event', (target, data) => {
    console.log(`target ${target} = ${data} `);
    event.check(target)
});

const { copy } = require('./libs/copy.js');
class Gyptio {
    async init() {
        var src, dest;
        var copy_project = commands.get('new')

        if (copy_project && copy_project.length == 2 && copy_project[0] == 'project') {
            try {
                event.emit('log', 'Copying files...')

                src = `${process.mainModule.path}/node_modules/@gyptio/quasar/default/project`
                dest = `./` + copy_project[1]

                // await copy(src, dest, () => {


                LoadInTerminal(`cp -r ${src} ${dest}`, function() {
                    event.emit('log', 'Install dependences npm...')
                    LoadInTerminal('npm i', function() {
                        event.emit('log', 'dependences npm installed!')
                        event.emit('event', "defaultsFiles", true);

                    }, { cwd: dest })
                })


                //})

            } catch (Exception) {
                console.log(Exception.message)
            }

        }
        var dev = commands.get('dev')
        if (dev && !dev[0]) {
            require(`@gyptio/vue`);
        }


        var new_serve = commands.get('new')
        if (new_serve) {
            LoadInTerminal(`cp -r ${src} ./buddies`, function() {

            })
        }


        var serve = commands.get('serve')
        if (serve) {
            src = `${__dirname}/projects/backend`
            console.log(`cp -r ${src} ./buddies`)

            LoadInTerminal(`cp -r ${src} ./buddies`, () => {
                require(`${process.cwd()}/buddies/index.js`);
            })
        }
    }
}

const gyption = new Gyptio();
gyption.init();
isRun = false

setTimeout(function() {
    if (event.runs.length === listEvents.length && !isRun) {

        process.on('SIGINT', function() {
            var x = 1
            console.log("Moment...");
            setInterval(function() {

                    console.log(x + "...")
                    if (x == 3) {
                        process.exit()
                    }
                    x++
                }, 1000)
                ///  process.exit();
        });

        isRun = true
        console.log("Success!")

        var cmd = `quasar dev`
        const o = exec(cmd, (error, stdout, stderr) => {
            pid = stdout;
            event.pid = pid
            console.log('pid ' + pid);
            if (error) {
                console.log(`error: ${error.message}`);
                return;
            }
            if (stderr) {
                console.log(`stderr: ${stderr}`);
                return;
            }
            console.log(`stdout: ${stdout}`);

        })

        console.log(o._handle.pid)

    }
}, 1000)